export interface HoverStyle {
	backgroundColor?: string;
	color?: string;
	durationIn?: number;
	durationOut?: number;
}